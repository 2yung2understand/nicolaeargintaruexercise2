package TaskManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable{
    private String firstName;
    private String lastName;
    private String userName;
    private List <Task> tasks = new ArrayList<>();

    public User() {}

    public User(String firstName, String lastName, String userName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserName() {
        return userName;
    }

    public String showUser() {
        return this.toString();
    }

    public String toString() {
        return firstName + " : " + lastName + " : " + userName;
    }

    public void addTask(String taskTitle, String taskDescription) {
        this.tasks.add(new Task(taskTitle, taskDescription));
    }

    public void showTasks(){
        for (Task task: this.tasks) {
            System.out.println(task);
        }
    }


}
