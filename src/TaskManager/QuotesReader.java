package TaskManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuotesReader {
    Pattern p = Pattern. compile("\'([^\"]*)\'");

    public QuotesReader() {
    }

    public String readQuotes (String line){
        while (line != null) {

            Matcher m = p.matcher(line);

            while (m.find()) {
                return m.group(1).trim();
            }
        }
        return null;
    }
}