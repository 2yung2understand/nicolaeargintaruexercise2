package TaskManager;


public class Main {
    public static void main(String[] args) {
        CommandLineParser clp = new CommandLineParser(args);
        UserManager um = new UserManager(clp);

        boolean isCreateUser = clp.getFlag("createUser");
        boolean isShowAllUsers = clp.getFlag("showAllUsers");
        boolean isAddTask = clp.getFlag("addTask");
        boolean isShowTasks = clp.getFlag("showTasks");

        if (isCreateUser) {
            um.createUser();
        }

        if (isShowAllUsers) {
            um.showAllUsers();
        }

        if (isAddTask) {
            um.addTask();
        }

        if (isShowTasks) {
            um.showTasks();
        }

     }
}
