package TaskManager;

import java.io.*;
import java.util.ArrayList;

public class UserManager {
    CommandLineParser clp;

    UserManager(CommandLineParser clp) {
        this.clp = clp;
    }

    public void createUser() {
        if (isEmptyOrDoesNotExist()) {
            try {
                FileOutputStream fos = new FileOutputStream("users.txt");
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                ArrayList <User> users = new ArrayList<User>();
                User user = new User(clp.getArgumentValue("fn"), clp.getArgumentValue("ln"), clp.getArgumentValue("un"));
                oos.writeObject(user);
                oos.close();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                FileInputStream fis = new FileInputStream("users.txt");
                ObjectInputStream ois = new ObjectInputStream(fis);
                ArrayList <User> users = new ArrayList<User>();
                users = (ArrayList <User>) ois.readObject();
                ois.close();
                fis.close();
                boolean isUnique = true;
                for (User u: users) {
                    if (u.getUserName().equals(clp.getArgumentValue("un"))) {
                        isUnique = false;
                    }
                }
                if (isUnique) {
                    FileOutputStream fos = new FileOutputStream("users.txt");
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    User user = new User(clp.getArgumentValue("fn"), clp.getArgumentValue("ln"), clp.getArgumentValue("un"));
                    users.add(user);
                    oos.writeObject(users);
                    oos.close();
                    fos.close();
                }

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


    private boolean isEmptyOrDoesNotExist() {
        File file = new File("users.txt");
        if (file.exists()) {
            if (file.length() == 0) {
                return true;
            } else return false;
        } else {
            return true;
        }
    }

    public void addTask() {
        try {
            FileInputStream fis = new FileInputStream("users.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList <User> users = new ArrayList<User>();
            users = (ArrayList<User>) ois.readObject();
            ois.close();
            fis.close();
            for (User user: users) {
                if (clp.getArgumentValue("un").equals(user.getUserName())) {
                    user.addTask(clp.getArgumentValue("tt"), clp.getArgumentValue("td"));
                    FileOutputStream fos = new FileOutputStream("users.txt");
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(users);
                    oos.close();
                    fos.close();
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void showTasks() {
        try {
            FileInputStream fis = new FileInputStream("users.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList <User> users = new ArrayList<User>();
            users = (ArrayList <User>) ois.readObject();
            ois.close();
            fis.close();
            for (User user: users) {
                if (user.getUserName().equals(clp.getArgumentValue("un"))) {
                    user.showTasks();
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

        public void showAllUsers() {
            ArrayList <User> users = new ArrayList<User>();
            try {
                FileInputStream fis = new FileInputStream("users.txt");
                ObjectInputStream ois = new ObjectInputStream(fis);
                users = (ArrayList <User>) ois.readObject();
                ois.close();
                fis.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            for (User user: users) {
                System.out.println(user);
            }
        }
    }




