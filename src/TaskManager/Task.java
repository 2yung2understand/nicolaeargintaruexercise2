package TaskManager;

import java.io.Serializable;

public class Task implements Serializable {
    private String taskTitle;
    private String taskDescription;


    public Task(String taskTitle, String taskDescription) {
        this.taskTitle = taskTitle;
        this.taskDescription = taskDescription;
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public String showTask() {
        return this.toString();
    }

    public String toString() {
        return taskTitle + " : " + taskDescription;
    }
}
