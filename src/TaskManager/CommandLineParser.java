package TaskManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandLineParser implements Serializable {
    List <String> args;
    List <String> argumentNames = new ArrayList<>();

    CommandLineParser(String arguments[])
    {
        this.args = Arrays.asList(arguments);
    }

    // Return argument names
    public List getArgumentNames()
    {
        for (String arg: args) {
            if (arg.startsWith("-")) {
                if (arg.contains("=")) {
                    String [] parts = arg.split("=");
                    argumentNames.add(parts[0]);
                } else {
                    argumentNames.add(arg);
                }
            }
        }
        return argumentNames;
    }

    // Check if flag is given
    public boolean getFlag(String flagName)
    {
        boolean result = false;
        for (String arg: args) {
            if (arg.startsWith("-") && arg.contains(flagName)) {
                    result = true;
                }
            }
        return result;
    }


    // Return argument value for particular argument name
    public String getArgumentValue(String argumentName)
    {
        QuotesReader qr = new QuotesReader();
        String result = null;
        for (String arg: args) {
            if (arg.startsWith("-") && arg.contains(argumentName)) {
                if (arg.contains("=")) {
                    String [] parts = arg.split("=");
                    result = qr.readQuotes(parts[1]);
                }
            }
        }
        return result;
    }
}
