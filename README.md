# Exercise 2

## Task Manager
You need to create a java application that will work as a task manager, with this application we should be able to do:
1. create new users (insert: FirstName, LastName, userName)
2. show all users (prin: FirstName, LastName, number of tasks)
3. add a task to the user (insert username, Task Title, Description)
4. show user's tasks (print: Task title, Description)
All data should be kept in the file, writing and reading should happen via serialization and deserilization operations. 

### Acceptance Criteria 
1. Create new users - by running this command:   
> java -jar myaplication.jar -createUser -fn='FirstName' -ln='LastName' -un='UserName'
UserName should be unique,  dot'n forget about validation
2. Show All Users - by running this command:  
> java -jar myaplication.jar -showAllUsers
4. Add a task to the user - by running this command:  
> java -jar myaplication.jar -addTask -un='userName' -tt='Task Title' -td='Task Description'
5. Show user's tasks - by running this command:  
> java -jar myaplication.jar -showTasks -un='userName'
